#!/usr/bin/perl

use strict;
use warnings;
use utf8;

use Carp ();
use Encode ();
use File::Basename ();
use File::Copy ();
use File::Path ();
use Getopt::Long ();
use IO::Select ();
use IPC::Open2 ();
use POSIX ();

use HTML::Entities ();

my $OPT_BASE_HREF = q(b);
my $OPT_BOTTRAP_HREF = q(p);
my $OPT_FILTER_COMMAND = q(c);
my $OPT_META_ROBOTS = q(r);
my $OPT_QUIET = q(q);
my $OPT_SOURCE_SUFFIX = q(s);
my $OPT_TITLE_REGEX = q(t);

my $DEFAULT_FILTER_COMMAND = q(nroff -x0 -Tlocale);
my $DEFAULT_SOURCE_SUFFIX = q(.roff);

my $FILENAME_INDEX = q(index);
my $FILE_SUFFIX_HTML = q(.html);

my $IPC_BYTES_WRITE = 512;
my $IPC_BYTES_READ = $IPC_BYTES_WRITE * 2;
my $IPC_WAIT_TIME_SEC = 0.01;

sub slurp_file
{
	my($path) = @_;
	1 == scalar(@_) or Carp::croak(q(argc));
	(defined($path) && !ref($path) && -f($path))
			or Carp::croak(q(path));
	my $fh = undef();
	if (!open($fh, q(<:encoding(UTF-8)), $path))
	{
		warn(qq(Failed to open file $path: !\n));
		return();
	}
	my @content = ();
	while (!eof($fh))
	{
		my $line = readline($fh);
		if (!defined($line))
		{
			warn(qq($!\n));
			close($fh);
			return();
		}
		push(@content, $line);
	}
	if (!close($fh))
	{
		warn(qq(Failed to close file $path: $!\n));
		return();
	}
	return(join(q(), @content));
}

# Filters `$source` through `$cmd` and returns the result.
sub filter
{
	my($source, $cmd) = @_;
	2 == scalar(@_) or Carp::croak(q(argc));
	(defined($source) && !ref($source)) or Carp::croak(q(source));
	(defined($cmd) && !ref($cmd)) or Carp::croak(q(cmd));
	local $SIG{q(PIPE)} = q(IGNORE);
	my($pid, $in, $out);
	goto RETURN_ERROR unless
		eval { $pid = IPC::Open2::open2($in, $out, $cmd); 1; };
	goto RETURN_ERROR unless binmode($in, q(:raw));
	goto RETURN_ERROR unless binmode($out, q(:raw));
	my @bytes_read = ();
	my $bytes_written = 0;
	my $select = IO::Select->new($in, $out);
	my $source_enc = Encode::encode(q(UTF-8), $source);
	while (scalar($select->handles()) > 0)
	{
		if ($select->exists($out)
		&& $select->can_write($IPC_WAIT_TIME_SEC))
		{
			my $len = syswrite($out, $source_enc,
				$IPC_BYTES_WRITE, $bytes_written);
			goto RETURN_ERROR unless defined($len);
			if ($len < 1)
			{
				goto RETURN_ERROR unless close($out);
				$select->remove($out);
			}
			$bytes_written += $len if $len > 0;
		}
		if ($select->exists($in)
		&& $select->can_read($IPC_WAIT_TIME_SEC))
		{
			my $len = sysread($in, my $bytes,
					$IPC_BYTES_READ);
			goto RETURN_ERROR unless defined($len);
			if ($len < 1)
			{
				goto RETURN_ERROR unless close($in);
				$select->remove($in);
			}
			push(@bytes_read, $bytes) if $len > 0;
		}
		goto RETURN_ERROR if $select->has_exception(0);
	}
	waitpid($pid, 0);
	my $child_exit_status = $? >> 8;
	return() unless $child_exit_status == 0;
	return(Encode::decode(q(UTF-8), join(q(), @bytes_read)));
RETURN_ERROR:
	warn(q(IPC error: ) . ($! ? $! : q(Unknown error)) . qq(\n));
	close($in) if defined($select) && $select->exists($in);
	close($out) if defined($select) && $select->exists($out);
	waitpid($pid, POSIX::WNOHANG) if defined($pid);
	return();
}

# Tries to extract the title from `$source` by looking for the middle
# part of the `.tl` element (or the regex supplied in `$opts`). Returns
# an empty string if not found.
sub extract_title
{
	my($opts, $source) = @_;
	2 == scalar(@_) or Carp::croak(q(argc));
	(defined($opts) && q(HASH) eq ref($opts))
			or Carp::croak(q(opts));
	(defined($source) && !ref($source)) or Carp::croak(q(source));
	my $regex = opt_title_regex($opts);
	if (defined($regex))
	{
		return($source =~ m/$regex/s ? $1 : q());
	}
	else
	{
		return($source =~ m/^\.tl '[^']*'([^']+)'[^']*'$/m
			|| $source =~ m/^\.tl "[^"]*"([^"]+)"[^"]*"$/m)
			? $1 : q();
	}
}

sub escape_html
{
	my($raw) = @_;
	1 == scalar(@_) or Carp::croak(q(argc));
	(defined($raw) && !ref($raw)) or Carp::croak(q(raw));
	return(HTML::Entities::encode_entities($raw));
}

sub html_base_element
{
	my($opts, $tgtpart) = @_;
	2 == scalar(@_) or Carp::croak(q(argc));
	(defined($opts) && q(HASH) eq ref($opts))
			or Carp::croak(q(opts));
	(defined($tgtpart) && q(ARRAY) eq ref($tgtpart)
			&& scalar(@{$tgtpart}) > 0)
			or Carp::croak(q(tgtpart));
	my $base_href_stem = opt_base_href($opts);
	return(q()) unless defined($base_href_stem);
	my $base_href = dirs_to_path([$base_href_stem,
			@{$tgtpart}[0..($#{$tgtpart} - 1)]]) . q(/);
	return(q(<BASE HREF=") . escape_html($base_href) . q(">));
}

sub html_meta_robots_element
{
	my($opts) = @_;
	1 == scalar(@_) or Carp::croak(q(argc));
	(defined($opts) && q(HASH) eq ref($opts))
			or Carp::croak(q(opts));
	return(opt_meta_robots($opts)
		? q(<META NAME="robots" CONTENT="noindex,nofollow">)
		: q());
}

sub html_begin
{
	my($opts, $title, $tgtpart) = @_;
	3 == scalar(@_) or Carp::croak(q(argc));
	(defined($opts) && q(HASH) eq ref($opts))
			or Carp::croak(q(opts));
	(defined($title) && !ref($title)) or Carp::croak(q(title));
	(defined($tgtpart) && q(ARRAY) eq ref($tgtpart)
			&& scalar(@{$tgtpart}) > 0)
			or Carp::croak(q(tgtpart));
	my $html_base_element = html_base_element($opts, $tgtpart);
	my $html_meta_robots_element = html_meta_robots_element($opts);
	my $html_title = escape_html($title);
	my $html = << "EOF";
<!DOCTYPE HTML PUBLIC "-//IETF//DTD HTML i18n//EN">
<HTML>
<HEAD>
<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=UTF-8">
$html_meta_robots_element
<TITLE>$html_title</TITLE>
$html_base_element
</HEAD>
<BODY>
<PRE>
EOF
	return($html);
}

sub html_bottrap_element
{
	my($opts, $depth) = @_;
	2 == scalar(@_) or Carp::croak(q(argc));
	(defined($opts) && q(HASH) eq ref($opts))
			or Carp::croak(q(opts));
	(defined($depth) && !ref($depth) && $depth > 0)
			or Carp::croak(q(depth));
	my $bottrap_href = opt_bottrap_href($opts);
	if (defined($bottrap_href))
	{
		my $html_bottrap_href = escape_html((q(../)
				x ($depth - 1)) . $bottrap_href);
		return(qq(<P><A HREF="$html_bottrap_href"></A></P>));
	}
	else
	{
		return(q());
	}
}

sub html_end
{
	my($opts, $depth) = @_;
	2 == scalar(@_) or Carp::croak(q(argc));
	(defined($opts) && q(HASH) eq ref($opts))
			or Carp::croak(q(opts));
	(defined($depth) && !ref($depth) && $depth > 0)
			or Carp::croak(q(depth));
	my $html_bottrap_element = html_bottrap_element($opts, $depth);
	my $html = << "EOF";
</PRE>
$html_bottrap_element
</BODY>
</HTML>
EOF
	return($html);
}

sub html
{
	my($opts, $source, $tgtpart) = @_;
	3 == scalar(@_) or Carp::croak(q(argc));
	(defined($opts) && q(HASH) eq ref($opts))
			or Carp::croak(q(opts));
	(defined($source) && !ref($source)) or Carp::croak(q(source));
	(defined($tgtpart) && q(ARRAY) eq ref($tgtpart)
			&& scalar(@{$tgtpart}) > 0)
			or Carp::croak(q(tgtpart));
	my $formatted = filter($source, opt_filter_command($opts));
	return() unless defined($formatted);
	my $title = extract_title($opts, $source);
	my $depth = scalar(@{$tgtpart});
	return(html_begin($opts, $title, $tgtpart)
			. $formatted
			. html_end($opts, $depth));
}

sub mkdir_recursive
{
	my($path) = @_;
	1 == scalar(@_) or Carp::croak(q(argc));
	(defined($path) && !ref($path)) or Carp::croak(q(path));
	my $errs = [];
	my $count = File::Path::make_path($path, {q(error) => \$errs});
	for my $err (@{$errs})
	{
		for my $dir (keys(%{$err}))
		{
			my $msg = $err->{$dir};
			warn(qq($msg: $dir\n));
		}
		return();
	}
	return(1);
}

sub overwrite
{
	my($path, $content) = @_;
	2 == scalar(@_) or Carp::croak(q(argc));
	(defined($path) && !ref($path)) or Carp::croak(q(path));
	(defined($content) && !ref($content))
			or Carp::croak(q(content));
	my $fh = undef();
	if (!open($fh, q(>:encoding(UTF-8)), $path))
	{
		warn(qq(Failed to open file $path: $!\n));
		return();
	}
	if (!print({$fh} $content))
	{
		warn(qq(Failed to write to file $path: $!\n));
		close($fh);
		return();
	}
	if (!close($fh))
	{
		warn(qq(Failed to close file $path: $!\n));
		return();
	}
	return(1);
}

sub copy_clobber
{
	my($srcpath, $tgtpath) = @_;
	2 == scalar(@_) or Carp::croak(q(argc));
	(defined($srcpath) && !ref($srcpath) && -f($srcpath))
			or Carp::croak(q(srcpath));
	(defined($tgtpath) && !ref($tgtpath))
			or Carp::croak(q(tgtpath));
	if (!File::Copy::cp($srcpath, $tgtpath))
	{
		warn(qq(Failed to copy to $tgtpath: $!\n));
		return();
	}
	return(1);
}

# Transfers the `$srcpart` file from `$srcdir` to `$tgtdir`, either by
# copying it or converting its content with the filter command.
sub transfer
{
	my($opts, $srcpart, $srcdir, $tgtdir) = @_;
	4 == scalar(@_) or Carp::croak(q(argc));
	(defined($opts) && q(HASH) eq ref($opts))
			or Carp::croak(q(opts));
	(defined($srcpart) && q(ARRAY) eq ref($srcpart)
			&& scalar(@{$srcpart}) > 0)
			or Carp::croak(q(srcpart));
	(defined($srcdir) && !ref($srcdir) && -d($srcdir))
			or Carp::croak(q(srcdir));
	(defined($tgtdir) && !ref($tgtdir)) or Carp::croak(q(tgtdir));
	my $src_filename = srcpart_filename($srcpart);
	my $srcpath = dirs_to_path([$srcdir, @{$srcpart}]);
	if (filename_is_html_source($opts, $src_filename))
	{
		my $source = slurp_file($srcpath);
		return() unless defined($source);
		my $tgtpart = tgtpart_html($srcpart,
				opt_source_extension($opts));
		my $html = html($opts, $source, $tgtpart);
		return() unless defined($html);
		my $tgtpath = dirs_to_path([$tgtdir, @{$tgtpart}]);
		my $tgtparent = dirs_to_path([$tgtdir,
				@{$tgtpart}[0..($#{$tgtpart} - 1)]]);
		return() unless mkdir_recursive($tgtparent);
		return() unless overwrite($tgtpath, $html);
		return($tgtpart);
	}
	else
	{
		my $tgtparent = dirs_to_path([$tgtdir,
				@{$srcpart}[0..($#{$srcpart} - 1)]]);
		return() unless mkdir_recursive($tgtparent);
		my $tgtpath = dirs_to_path([$tgtdir, @{$srcpart}]);
		return() unless copy_clobber($srcpath, $tgtpath);
		return($srcpart);
	}
}

sub filename_is_html_source
{
	my($opts, $filename) = @_;
	2 == scalar(@_) or Carp::croak(q(argc));
	(defined($opts) && q(HASH) eq ref($opts))
			or Carp::croak(q(opts));
	(defined($filename) && !ref($filename))
			or Carp::croak(q(filename));
	my $src_extension = opt_source_extension($opts);
	return($filename =~ m/\Q$src_extension\E$/s);
}

sub srcpart_filename
{
	my($srcpart) = @_;
	1 == scalar(@_) or Carp::croak(q(argc));
	(defined($srcpart) && q(ARRAY) eq ref($srcpart)
		&& scalar(@{$srcpart}) > 0) or Carp::croak(q(srcpart));
	return($srcpart->[$#{$srcpart}]);
}

sub filename_basename
{
	my($filename, $extension) = @_;
	2 == scalar(@_) or Carp::croak(q(argc));
	(defined($filename) && !ref($filename))
			or Carp::croak(q(filename));
	(defined($extension) && !ref($extension))
			or Carp::croak(q(extension));
	my $basename = File::Basename::fileparse($filename, $extension);
	return($basename);
}

# Returns the target path components for `$srcpart`. If the basename of
# the source file is `index`, then it will renamed to `index.html` in
# the target path. Otherwise a directory is created in the target path,
# whose name is the basename of the file, and the filename becomes
# `index.html`. Examples:
# *	`src/index.roff -> tgt/index.html`
# *	`src/foo.roff -> tgt/foo/index.html`
sub tgtpart_html
{
	my($srcpart, $extension) = @_;
	2 == scalar(@_) or Carp::croak(q(argc));
	(defined($srcpart) && q(ARRAY) eq ref($srcpart)
			&& scalar(@{$srcpart}) > 0)
			or Carp::croak(q(srcpart));
	(defined($extension) && !ref($extension))
			or Carp::croak(q(extension));
	my $filename = srcpart_filename($srcpart);
	my $basename = filename_basename($filename, $extension);
	if ($basename eq $FILENAME_INDEX)
	{
		my @tgtpart = @{$srcpart};
		$tgtpart[$#tgtpart] = $basename . $FILE_SUFFIX_HTML;
		return(\@tgtpart);
	}
	else
	{
		my @tgtpart = @{$srcpart};
		$tgtpart[$#tgtpart] = $basename;
		push(@tgtpart, $FILENAME_INDEX . $FILE_SUFFIX_HTML);
		return(\@tgtpart);
	}
}

# Returns whether the file's mtime has changed.
sub file_outdated
{
	my($srcpath, $tgtpath) = @_;
	2 == scalar(@_) or Carp::croak(q(argc));
	(defined($srcpath) && !ref($srcpath) && -f($srcpath))
			or Carp::croak(q(srcpath));
	(defined($tgtpath) && !ref($tgtpath))
			or Carp::croak(q(tgtpath));
	return(1) unless -f($tgtpath);
	my @srcstat = stat($srcpath);
	if (scalar(@srcstat) < 1)
	{
		warn(qq($!\n));
		return();
	}
	my @tgtstat = stat($tgtpath);
	if (scalar(@tgtstat) < 1)
	{
		warn(qq($!\n));
		return();
	}
	return($srcstat[9] > $tgtstat[9] ? 1 : 0);
}

# Returns true if the file does not exist in the target directory or if
# the source file has a newer mtime.
sub should_transfer
{
	my($opts, $srcpart, $srcdir, $tgtdir) = @_;
	4 == scalar(@_) or Carp::croak(q(argc));
	(defined($opts) && q(HASH) eq ref($opts))
			or Carp::croak(q(opts));
	(defined($srcpart) && q(ARRAY) eq ref($srcpart)
			&& scalar(@{$srcpart}) > 0)
			or Carp::croak(q(srcpart));
	(defined($srcdir) && !ref($srcdir) && -d($srcdir))
			or Carp::croak(q(srcdir));
	(defined($tgtdir) && !ref($tgtdir)) or Carp::croak(q(tgtdir));
	my $src_filename = srcpart_filename($srcpart);
	my $srcpath = dirs_to_path([$srcdir, @{$srcpart}]);
	if (filename_is_html_source($opts, $src_filename))
	{
		my $tgtpart = tgtpart_html($srcpart,
				opt_source_extension($opts));
		my $tgtpath = dirs_to_path([$tgtdir, @{$tgtpart}]);
		return(file_outdated($srcpath, $tgtpath));
	}
	else
	{
		my $tgtpath = dirs_to_path([$tgtdir, @{$srcpart}]);
		return(file_outdated($srcpath, $tgtpath));
	}
}

sub slurp_dir
{
	my($path) = @_;
	1 == scalar(@_) or Carp::croak(q(argc));
	(defined($path) && -d($path)) or Carp::croak($path);
	my $dh = undef();
	if (!opendir($dh, $path))
	{
		warn(qq(Failed to open directory $path: $!\n));
		return();
	}
	my @dirs = readdir($dh);
	if (!closedir($dh))
	{
		warn(qq(Failed to close directory $path: $!\n));
		return();
	}
	return(\@dirs);
}

sub dirs_to_path
{
	my($srcpart) = @_;
	1 == scalar(@_) or Carp::croak(q(argc));
	(defined($srcpart) && q(ARRAY) eq ref($srcpart)
			&& scalar(@{$srcpart}) > 0)
			or Carp::croak(q(srcpart));
	return(join(q(/), @{$srcpart}));
}

# Returns an arrayref of arrayrefs, where each element is a path
# component. The last element is always a file and the previous elements
# are always directories. Example:
# `'dir1/dir2/file.txt' -> ['dir1', 'dir2', 'file.txt']`
sub traverse_dir
{
	my($basedir, $subdirs) = @_;
	2 == scalar(@_) or Carp::croak(q(argc));
	(defined($basedir) && !ref($basedir) && -d($basedir))
			or Carp::croak(q(basedir));
	(defined($subdirs) && q(ARRAY) eq ref($subdirs))
			or Carp::croak(q(subdirs));
	my $srcdir = dirs_to_path([$basedir, @{$subdirs}]);
	my $filenames = slurp_dir($srcdir);
	return() unless defined($filenames);
	my @srcfiles = ();
	foreach my $filename (sort(@{$filenames}))
	{
		if (-f(qq($srcdir/$filename)))
		{
			push(@srcfiles, [@{$subdirs}, $filename]);
		}
		elsif (-d(qq($srcdir/$filename))
		&& $filename !~ m/^\.$/s
		&& $filename !~ m/^\.\.$/s)
		{
			my $subfiles = traverse_dir($basedir,
					[@{$subdirs}, $filename]);
			return() unless defined($subfiles);
			push(@srcfiles, @{$subfiles});
		}
	}
	return(\@srcfiles);
}

sub opt_base_href
{
	my($opts) = @_;
	1 == scalar(@_) or Carp::croak(q(argc));
	(defined($opts) && q(HASH) eq ref($opts))
			or Carp::croak(q(opts));
	return(exists($opts->{$OPT_BASE_HREF})
			? $opts->{$OPT_BASE_HREF} : undef());
}

sub opt_bottrap_href
{
	my($opts) = @_;
	1 == scalar(@_) or Carp::croak(q(argc));
	(defined($opts) && q(HASH) eq ref($opts))
			or Carp::croak(q(opts));
	return(exists($opts->{$OPT_BOTTRAP_HREF})
			? $opts->{$OPT_BOTTRAP_HREF} : undef());
}

sub opt_filter_command
{
	my($opts) = @_;
	1 == scalar(@_) or Carp::croak(q(argc));
	(defined($opts) && q(HASH) eq ref($opts))
			or Carp::croak(q(opts));
	return(exists($opts->{$OPT_FILTER_COMMAND})
			? $opts->{$OPT_FILTER_COMMAND}
			: $DEFAULT_FILTER_COMMAND);
}

sub opt_meta_robots
{
	my($opts) = @_;
	1 == scalar(@_) or Carp::croak(q(argc));
	(defined($opts) && q(HASH) eq ref($opts))
			or Carp::croak(q(opts));
	return(exists($opts->{$OPT_META_ROBOTS}) ? 1 : undef());
}

sub opt_quiet
{
	my($opts) = @_;
	1 == scalar(@_) or Carp::croak(q(argc));
	(defined($opts) && q(HASH) eq ref($opts))
			or Carp::croak(q(opts));
	return(exists($opts->{$OPT_QUIET}) ? 1 : undef());
}

sub opt_source_extension
{
	my($opts) = @_;
	1 == scalar(@_) or Carp::croak(q(argc));
	(defined($opts) && q(HASH) eq ref($opts))
			or Carp::croak(q(opts));
	return(exists($opts->{$OPT_SOURCE_SUFFIX})
			? $opts->{$OPT_SOURCE_SUFFIX}
			: $DEFAULT_SOURCE_SUFFIX);
}

sub opt_title_regex
{
	my($opts) = @_;
	1 == scalar(@_) or Carp::croak(q(argc));
	(defined($opts) && q(HASH) eq ref($opts))
			or Carp::croak(q(opts));
	return(exists($opts->{$OPT_TITLE_REGEX})
			? $opts->{$OPT_TITLE_REGEX} : undef());
}

sub usage
{
	0 == scalar(@_) or Carp::croak(q(argc));
	return(qq($0) . qq( [-$OPT_BASE_HREF HREF])
			. qq( [-$OPT_BOTTRAP_HREF HREF])
			. qq( [-$OPT_FILTER_COMMAND CMD])
			. qq( [-$OPT_META_ROBOTS])
			. qq( [-$OPT_QUIET])
			. qq( [-$OPT_SOURCE_SUFFIX STR])
			. qq( [-$OPT_TITLE_REGEX REGEX])
			. q( SRCDIR TGTDIR));
}

sub getopts_options
{
	0 == scalar(@_) or Carp::croak(q(argc));
	return(qq($OPT_BASE_HREF:s),
			qq($OPT_BOTTRAP_HREF:s),
			qq($OPT_FILTER_COMMAND:s),
			$OPT_META_ROBOTS,
			$OPT_QUIET,
			qq($OPT_SOURCE_SUFFIX:s),
			qq($OPT_TITLE_REGEX:s));
}

sub getopts
{
	my($argv) = @_;
	1 == scalar(@_) or Carp::croak(q(argc));
	(defined($argv) && q(ARRAY) eq ref($argv))
			or Carp::croak(q(argv));
	Getopt::Long::Configure(q(bundling));
	my @remaining = @{$argv};
	my %opts = ();
	Getopt::Long::GetOptionsFromArray(\@remaining, \%opts,
			getopts_options());
	if (scalar(@remaining) != 2)
	{
		warn(usage() . qq(\n));
		return();
	}
	my $srcdir = shift(@remaining);
	my $tgtdir = shift(@remaining);
	return(\%opts, $srcdir, $tgtdir);
}

sub main
{
	my($argv) = @_;
	1 == scalar(@_) or Carp::croak(q(argc));
	(defined($argv) && q(ARRAY) eq ref($argv))
			or Carp::croak(q(argv));
	my($opts, $srcdir, $tgtdir) = getopts($argv);
	return() unless defined($opts);
	my $srcparts = traverse_dir($srcdir, []);
	return() unless defined($srcparts);
	foreach my $srcpart (@{$srcparts})
	{
		my $outdated = should_transfer
				($opts, $srcpart, $srcdir, $tgtdir);
		return() unless defined($outdated);
		if ($outdated)
		{
			my $tgtpart = transfer($opts, $srcpart,
					$srcdir, $tgtdir);
			return() unless defined($tgtpart);
			(print(dirs_to_path($tgtpart) . qq(\n))
					or return())
					unless opt_quiet($opts);
		}
	}
	return(1);
}

exit(main(\@ARGV) ? 0 : 1);

__END__

=pod

=encoding utf8

=head1 Name

roffsite - pre-formatted roff static website generator

=head1 Synopsis

roffsite [-b HREF] [-c CMD] [-p HREF] [-q] [-r] [-s STR] [-t REGEX]
	SRCDIR TGTDIR

=head1 Description

roffsite is a static website generator that creates HTML files from
source files by running them through a filter such as nroff. SRCDIR is
the source directory and TGTDIR is the target HTML directory.

=head1 Options

=over

=item -b HREF: Insert a BASE element with HREF as the HREF attribute.

=item -c CMD: Filter command. Default: "nroff -x0 -Tlocale"

=item -q: Quiet. The default is to print every updated output file.

=item -r: Insert a "noindex,nofollow" robots META tag.

=item -p HREF: Generate a bot trap link with HREF as the HREF attribute.

=item -s STR: Source file suffix. Default: ".roff"

=item -t REGEX: Regular expression to override title extraction.

=back

=head1 HTML conversion

Source files are converted using the filter command. The generated text
is inserted into a PRE element. The generated HTML is internationalized
HTML 2 (IETF RFC 2070).

=head1 Title extraction

The TITLE element is created by looking for the middle part of the ".tl"
roff command. For example:

	.tl ''The title''

Becomes:

	<TITLE>The title</TITLE>

You can override this with your own regular expression. If so, the REGEX
must be a standard Perl regular expression with at least one capturing
group (only the first capturing group will be used). The REGEX will be
applied to the entire content of the source file using the "/s"
modifier. The default is to match either of the following:

	m/^\.tl '[^']*'([^']+)'[^']*'$/m
	m/^\.tl "[^"]*"([^"]+)"[^"]*"$/m

=head1 Directory structure

There are two types of files: source files and regular files. Source
files are identified by their filename suffixes.

Regular files are copied from the source directory to the target
directory as is (without changing the relative path).

Source files are always named "index.html" in the target directory. If
the basename of a source file is "index", then its suffix is just
changed to ".html". Otherwise the file is placed in a directory whose
name is the basename of the source file.

Examples:

	SRCDIR/index.txt -> TGTDIR/index.html
	SRCDIR/foo.txt -> TGTDIR/foo/index.html
	SRCDIR/foo/index.txt -> TGTDIR/foo/index.html
	SRCDIR/foo/bar.txt -> TGTDIR/foo/bar/index.html

=head1 Bot trap

A bot trap is an empty P element with an empty A element whose HREF is
the bot trap HREF. The bot trap HREF is relative to the web root. If the
bot trap HREF is "admin.cgi", then every HTML file will refer to a file
named "admin.cgi" in the web root.

The purpose is to insert an invisible link that robots and low-lives
will go to. This link can be a CGI script that bans their IP addresses.

=head1 Exit status

0 on success, 1 on any failure.

=head1 Examples

Create an unformatted plain text website (".txt" source files and the
first line becomes the title):

	roffsite -c cat -s '.txt' -t '^([^\n]+)\n'

Create a Markdown website (".md" source files and "# ..." as the title):

	roffsite -c markdown -s '.md' -t '^# ([^\n]+)\n'

=head1 Dependencies

=over

=item HTML::Entities

=item nroff(1) (or other filter command)

=back

=head1 Bugs and limitations

roffsite assumes that all source files are UTF-8 encoded. Use iconv(1)
to get rid of idiot encodings. Some programs, like groff, are broken by
default and require you to set some environment variable to make them
work like they should.

	GROFF_ENCODING=utf-8

Heirloom nroff is officially endorsed. Use Heirloom nroff for a
stress-free life.

The pipe buffer must be at least 512 bytes for the filter command
(POSIX.1-2001).

=head1 See also

nroff(1), lynx(1), perlre(1)

=head1 Homepage

tafl.se/roffsite

=head1 Author

Alexander Söderlund (soderlund at tafl dot se)

=head1 Copyright

Copyright © 2018 Alexander Söderlund (Sweden)

Permission to use, copy, modify, and distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

