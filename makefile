.POSIX:

PRGNAM = roffsite
VERSION = 181110

LINT = perlcritic
LINTFLAGS = --noprofile --nocolor --severity 1\
	--exclude ProhibitEscapedMetaCharacters\
	--exclude ProhibitExcessComplexity\
	--exclude ProhibitMagicNumbers\
	--exclude ProhibitParensWithBuiltins\
	--exclude ProhibitPostfixControls\
	--exclude ProhibitPunctuationVars\
	--exclude RequireArgUnpacking\
	--exclude RequireBriefOpen\
	--exclude RequireCheckedClose\
	--exclude RequireCheckedSyscalls\
	--exclude RequireDotMatchAnything\
	--exclude RequireExtendedFormatting\
	--exclude RequireLineBoundaryMatching\
	--exclude RequireNegativeIndices\
	--exclude RequirePodSections\
	--exclude RequireTidyCode\
	--exclude RequireVersionVar

SRCDIR = src
BIN = $(SRCDIR)/$(PRGNAM).pl
TESTDIR = test
TESTSRCDIR = $(TESTDIR)/src
TESTOBJDIR = $(TESTDIR)/obj

all:

clean:
	rm -rf $(TESTOBJDIR)

mrproper: clean

dist: mrproper
	mkdir '$(PRGNAM)-$(VERSION)'
	find . -maxdepth 1 -mindepth 1\
		-and -not -name '$(PRGNAM)-$(VERSION)'\
		-and -not -name '.git'\
		-exec cp -r '{}' '$(PRGNAM)-$(VERSION)' \;
	tar -cf '$(PRGNAM)-$(VERSION).tar' '$(PRGNAM)-$(VERSION)'
	gzip -9 '$(PRGNAM)-$(VERSION).tar'
	rm -r '$(PRGNAM)-$(VERSION)'

lint:
	$(LINT) $(LINTFLAGS) $(BIN)

test: test-default test-markdown test-plain

test-default:
	$(BIN) -b 'https://example.com' -p 'admin.cgi' -r\
			$(TESTSRCDIR)/default $(TESTOBJDIR)/default
	test -f '$(TESTOBJDIR)/default/index.html'
	test -f '$(TESTOBJDIR)/default/not_a_source_file.txt'
	test -f '$(TESTOBJDIR)/default/test1/bar/baz/index.html'
	test -f '$(TESTOBJDIR)/default/test1/bar/index.html'
	test -f '$(TESTOBJDIR)/default/test1/foo/index.html'
	test -f '$(TESTOBJDIR)/default/test1/index.html'
	test -f '$(TESTOBJDIR)/default/test1/not_a_source_file'
	test -f '$(TESTOBJDIR)/default/test2/blutt/index.html'
	test -f '$(TESTOBJDIR)/default/test3/index.html'
	diff '$(TESTSRCDIR)/default/not_a_source_file.txt'\
		'$(TESTOBJDIR)/default/not_a_source_file.txt'
	diff '$(TESTSRCDIR)/default/test1/not_a_source_file'\
		'$(TESTOBJDIR)/default/test1/not_a_source_file'

test-markdown:
	$(BIN) -c cat -s '.md' -t '^# ([^\n]+)\n'\
			$(TESTSRCDIR)/markdown $(TESTOBJDIR)/markdown
	test -f '$(TESTOBJDIR)/markdown/index.html'

test-plain:
	$(BIN) -c cat -s '.txt' -t '^([^\n]+)\n'\
			$(TESTSRCDIR)/plain $(TESTOBJDIR)/plain
	test -f '$(TESTOBJDIR)/plain/index.html'

FORCE:

